# In the original file you repeated yourself a lot in terms of what code blocks
# you were using. The numbers were slightly different, but the underlying 
# algorithm was identical. While at the end of the day the computer will execute
# the exact same set of instructions, utilizing a function reduces the chance
# of human error along the way. Say for example when copy-pasting the algorithm
# you accidentally missed a negative sign (like I always used to on math quizzes)
# It could potentially be hard to find that bug. Using functions eliminates that
# risk and makes your code more robust. The technical term for this is "abstraction"

import codrone_edu
# import mock_drone

# Setting the drone interface variable the proper way. This is functionally no
# different than what the original program is, but its visually clearer on
# what is going on and where things are coming from. At the top of the program
# we imported a module called codrone_edu and the "." operator is saying "within
# this module there is blahblahblah". Just like before we're saying we want to
# create an instance of a drone interface, but this time we're saying
# where to find / what we want explicitly. You can read it as
# codrone_edu module drone module Drone

drone = codrone_edu.drone.Drone()
# drone = mock_drone.Drone()
drone.open()

# Python typically tries to have its syntax designed around plain English, but
# pulls in a lot of design elements from the C programming language which is
# about as far from English as you can get. Function signatures are closer to
# C unfortunatly. In English we're saying that we are "defining" a function
# called "go_forward_until". Just like mathematical functions, they can take
# arguments. I like to follow the programming philosophy of "self documenting
# code" so the argument names try to describe what their purpose is without
# needing too much extra information

def go_forward_until(target_distance_inches):

    distance = drone.get_front_range("in")
    while distance > target_distance_inches:
        drone.go(0, 50, 0, 0, 0.75)
        distance = drone.get_front_range("in")
        print("Forward = " + str(distance))
    drone.hover(2)

def go_reverse_until(target_distance_inches):

    # Whenever we create a new function, we enter a new "scope block". This
    # means that whatever variables we create here are "local" to this "scope"
    # only. In other words, the variable distance here has no correlation with
    # the variable distance in the "go_forward_until" function. However, variables
    # created outside of scope blocks are automatically brought in, so we can
    # work with the drone variable we created above and it references the same
    # one. Every time you make a new indentation within python, you create a new
    # scope block

    distance = drone.get_front_range("in")
    while distance < target_distance_inches:
        drone.go(0, -50, 0, 0, 0.75)
        distance = drone.get_front_range("in")
        print("Reverse = " + str(distance))
    drone.hover(2)

def go_up_until(target_distance_ft):

    distance = drone.get_elevation("ft")
    while distance < target_distance_ft:
        drone.go(0, 0, 0, 50, 1)
        distance = drone.get_elevation("ft")
        print("Height = " + str(distance))
    drone.hover(2)

# In Python, you have to define functions before you use them. Since we want to
# use those functions in the "body" of our program, we define them first and
# now we actually start writing the program
drone.reset_sensor()
drone.takeoff()
drone.reset_sensor()

# This here is what I think the largest benefit of using functions comes into
# play. When writing the flight script, the motions we want to have happen, we
# no longer have to pay attention to exactly _how_ these goals are accomplished
# we instead just say things in more or less plain english of what we want to
# have happen. In other words, here we are telling the drone in language that is
# easy for us to understand. 
go_up_until(9)

go_forward_until(30)

drone.turn_left(180)
go_forward_until(30)

drone.turn_left(180)
go_forward_until(30)
go_reverse_until(78)

# You can cascade functions to help eliminate variables when you don't really
# need them. Helps on readability
print("Land " + str(drone.get_front_range("in")))

drone.land()
drone.close()