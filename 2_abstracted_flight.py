# The biggest (theoretical) issue with function_flight is that while it made
# things more readable, it still had a lot of copy-pasted code. This flight
# program aims to reduce that level of copy-paste

import codrone_edu
# import mock_drone
drone = codrone_edu.drone.Drone()
# drone = mock_drone.Drone()
drone.open()

# The go_x_until functions still repeated a lot the same general idea of what
# to do, but there are slight differences. This new go_until function aims to
# generalize those functions so we have one function to maintain 

def go_until(direction, target_distance_inches):

    # Its common practice to assign what are effectively random numbers
    # symbolic names to help a read understand what is actually happening. In
    # the other flight programs, I eventually picked up on that the "50" in the
    # go function was meaning use 50% power forward and that -50 meant 50%
    # power reverse, but it wasn't immediately obvious. With giving things
    # symbolic names, a reader can make the assumption the underlying numeric
    # value does actually match the name and then when that name is used, it is
    # clearer what is happening at a glance.

    DIRECTION_POWER_LEVEL = 50
    FORWARD_POWER         = DIRECTION_POWER_LEVEL
    REVERSE_POWER         = -1 * DIRECTION_POWER_LEVEL
    INCHES_PER_FEET       = 12
    MOVEMENT_DURATION_S   = 0.75

    # This style of associating different arguments with configuring variables
    # for later use is usually frowned upon since it can be hard to read once
    # the list starts getting long. However, its really easy to understand the
    # pattern. The "elif" keyword is just simply short of "else if" and its
    # used because it allows the Python interpreter to jump to the end if it
    # finds a successful case. In other words, if the "direction == DOWN" case
    # is true and acted on, then none of the subsequent direction tests will
    # happen (although direction == UP test still would have happened since it
    # comes first). Tends to speed up code.

    roll_power     = 0
    pitch_power    = 0
    yaw_power      = 0
    throttle_power = 0

    if direction == "UP":
        throttle_power = FORWARD_POWER
    elif direction == "DOWN":
        throttle_power = REVERSE_POWER
    elif direction == "FORWARD":
        pitch_power = FORWARD_POWER
    elif direction == "BACKWARD":
        pitch_power = REVERSE_POWER

    # Skipping the rest of the directions since we don't need them right now
    # and it would make this function unweildy
    
    target_reached = False
    # The placement of "not" is another C-ism within Python. What we're saying
    # here is while the target_reached is not true
    while(not target_reached):

        # Depending on what direction we're moving, figure out the distance in
        # that direction
        distance_in = 0
        if direction == "UP" or direction == "DOWN":
            distance_in = drone.get_elevation("ft") * INCHES_PER_FEET
        elif direction == "FORWARD" or direction == "BACKWARD":
            distance_in = drone.get_front_range("in")

        # There is a lot of logic here
        if ( ((direction == "UP"   or direction == "BACKWARD") and distance_in > target_distance_inches) or 
             ((direction == "DOWN" or direction == "FORWARD")  and distance_in < target_distance_inches) ):
            target_reached = True
        else:
            drone.go(roll_power, pitch_power, yaw_power, throttle_power, MOVEMENT_DURATION_S)


        # A more typical way of creating dynamically generated console prints.
        # Each {} pair indicates that there is a variable to be filled in. The
        # :5.1f in the second one explicitly is telling python to expect a
        # floating point number and when rendering the number, use 5 character
        # places reserving 1 space for the decimal portion of the number. This
        # will also round so 0.0999 will show up as 0.1. Since the decimal
        # point takes up one of the character spots, we're actually only going
        # to get at most 3 digits ahead of the decimal point
        print("{} = {:5.1f}".format(direction, distance_in))
    drone.hover(2)

# Basically same as before
drone.reset_sensor()
drone.takeoff()
drone.reset_sensor()

# This function only takes in inches for the distance argument, so we have to
# multiply our old 9 to this
go_until("UP", 9 * 12) 

go_until("FORWARD", 30)

drone.turn_left(180)
go_until("FORWARD", 30)

drone.turn_left(180)
go_until("FORWARD",  30)
go_until("BACKWARD", 78)

print("Land {:5.1f}".format(drone.get_front_range("in")))

drone.land()
drone.close()