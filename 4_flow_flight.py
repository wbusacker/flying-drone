# Here we are, finally using the flow api. The reason I created so many
# alternate programs is that while making the function calls to get the flow
# sensor's data is trivial, using that data effectively is not. Your original
# style of controlling position would have failed you beyond 2, maybe 3
# position commands with the flow position functions. Without first ensuring
# that you are at the actual position you wanted to be at, each new command
# would have brought in new positional error which would add up across time. No
# matter how hard you tried to tune how fast you moved, what positional targets
# you wanted to reach, you would have eventually been extremely far off. In
# technical terms we call this "drift", its almost always categorized as random
# variables since each new run/attempt will give you new error accumulations,
# and in the world of system/vehicle control it is by far the hardest problem
# to solve. Even ULA's most accurate rockets in the world could not solve this
# problem and simply limited mission duration such that the maximum drift in
# that time period was below what the mission would require. That isn't trade
# secret knowledge, its just a known fact in this particular corner of the world. 
# 
# Welcome to Guidance/Navigation/Control (GNC for short).

import codrone_edu
# import mock_drone
drone = codrone_edu.drone.Drone()
# drone = mock_drone.Drone()
drone.open()
class Controller:
    PROPORTIONAL_GAIN  = 1
    INTEGRAL_GAIN      = 0.1
    INTEGRAL_THRESHOLD = 5

    def __init__(self):
        self.integrator = 0

    def generate_response(self, distance_remaining):
        if (abs(distance_remaining) < Controller.INTEGRAL_THRESHOLD):
            self.integrator += distance_remaining
        else:
            self.integrator = 0
        
        controler_response = distance_remaining * Controller.PROPORTIONAL_GAIN
        controler_response += self.integrator * Controller.INTEGRAL_GAIN

        return controler_response

def go_target(target_distance_inches, axis):

    POSITION_REACHED_THRESHOLD_INCHES = 0.25
    POWER_OUTPUT_CAP                  = 100
    FORWARD_POWER_OUTPUT_CAP          = POWER_OUTPUT_CAP
    REVERSE_POWER_OUTPUT_CAP          = -1 * POWER_OUTPUT_CAP
    INCHES_PER_FEET                   = 12
    MOVEMENT_DURATION_S               = 0.25

    target_reached = False

    power_controller = Controller()

    # I'm making an assumption here about how the get_flow functions work. They
    # read to me like they give you the distance the drone traveled since the
    # last call. At least I'm really hoping thats what it does because if it
    # gives you the distance traveled along the vehicle's X axis, then using
    # this function with turning will get _real_ interesting (read virtually
    # impossible). 
    current_distance_in = 0

    while(not target_reached):
        
        roll_power     = 0
        pitch_power    = 0
        yaw_power      = 0
        throttle_power = 0

        if axis == "X":
            # We need to accumulate the X and Y axis position
            current_distance_in += drone.get_flow_x("in")
        elif axis == "Y":
            current_distance_in += drone.get_flow_y("in")
        elif axis == "Z":
            current_distance_in = drone.get_elevation("ft") * INCHES_PER_FEET

        distance_remaining = target_distance_inches - current_distance_in

        if(abs(distance_remaining) < POSITION_REACHED_THRESHOLD_INCHES):
            target_reached = True
        else:

            movement_power_level = power_controller.generate_response(distance_remaining)

            # Limit the power level
            if (movement_power_level > FORWARD_POWER_OUTPUT_CAP):
                movement_power_level = FORWARD_POWER_OUTPUT_CAP
            elif (movement_power_level < REVERSE_POWER_OUTPUT_CAP):
                movement_power_level = REVERSE_POWER_OUTPUT_CAP

            if axis == "X":
                pitch_power = movement_power_level
            elif axis == "Y":
                roll_power = movement_power_level
            elif axis == "Z":
                throttle_power = movement_power_level
            
            drone.go(roll_power, pitch_power, yaw_power, throttle_power, MOVEMENT_DURATION_S)

            # Adding in the movement response so you can see how it changes across time
            print("{} -> {:5.1f} & {:5.1f}".format(axis, current_distance_in, movement_power_level))

# Basically same as before
drone.reset_sensor()
drone.takeoff()
drone.reset_sensor()

# This function only takes in inches for the distance argument, so we have to
# multiply our old 9 to this
go_target(9 * 12, "Z") 


# Unlike before where we would travel 30 inches away from the wall, we now only
# move 30 inches forward at a time
go_target(30, "X")

drone.turn_left(180)
go_target(30, "X")

drone.turn_left(180)
go_target(30, "X")

# This function call will still make the drone move backwards, but we no longer
# have to specify the motion and the axis anymore. We just do position on the
# axis and it will go there, regardless of where the drone currently is on the
# axis
go_target(78, "X")

print("Land {:5.1f}".format(drone.get_front_range("in")))

drone.land()
drone.close()