###############################################################################
#                                                                             #
#                                TEST SECTION                                 #
#                                                                             #
###############################################################################

from mock_drone import Quaternion, Dynamics, Drone
import math
import pytest

def test_rpy_to_quaternion():

    q = Quaternion()

    q.rotate_by_rpy(math.radians(45), 0, 0)

    assert(q.w == pytest.approx(0.9238795))
    assert(q.x == pytest.approx(0.3826834))
    assert(q.y == pytest.approx(0))
    assert(q.z == pytest.approx(0))

def test_quaternion_to_rpy():

    q = Quaternion()

    q.rotate_by_rpy(math.radians(45), 0, 0)

    rpy = q.get_rpy()

    assert(rpy[Dynamics.ROLL] == pytest.approx(math.radians(45)))
    assert(rpy[Dynamics.PITCH] == pytest.approx(0))
    assert(rpy[Dynamics.YAW] == pytest.approx(0))

def test_set_hover():

    drone = Drone()

    # Force assert the drone's XY location
    drone.dynamics.position[Dynamics.X_AXIS] = 40
    drone.dynamics.position[Dynamics.Y_AXIS] = 20

    # Let the drone fly up for a second
    drone.go(0, 0, 0, 100, 5)

    # Let the drone reach a steady state
    drone.go(0, 0, 0, 0, 5)

    # Capture the current Z axis position
    current_z = drone.dynamics.position[Dynamics.Z_AXIS]
    assert(current_z != pytest.approx(0))

    # Make sure our acceleration has zeroed out
    assert(abs(drone.dynamics.acceleration[Dynamics.Z_AXIS]) < 1e-3)
    assert(abs(drone.dynamics.velocity[Dynamics.Z_AXIS]) < 1e-3)