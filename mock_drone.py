#!/usr/bin/python3

import math
import numpy
import matplotlib.pyplot as plt
import time

UNIVERSE_SIMULATION_FREQUENCY = 1000
UNIVERSE_SIMULATION_PERIOD_S  = 1.0 / UNIVERSE_SIMULATION_FREQUENCY
UNIVERSE_DIMENSIONS           = 3

# Collect data every n simulation periods         n
SIM_LOG_PERIOD_S = 0.001

# Pulled this from ISO-5344:2004-3.17
STANDARD_GRAVITY = 9.80665

# Full precision known conversions 
CM_PER_INCHES    = 2.54
INCHES_PER_FEET  = 12
CM_PER_METER     = 100

# Derived conversions that have too many digits of precision for me to write so I just calculate them on the fly
FEET_PER_METER   = CM_PER_METER / (CM_PER_INCHES * INCHES_PER_FEET)
INCHES_PER_METER = CM_PER_METER / CM_PER_INCHES

FLOAT_FORMAT  = "{:15.6f}, "
STRING_FORMAT = "{:>15s}  "

global iteration_name
iteration_name = ""

class Quaternion:
    def __init__(self):

        self.set_to_rpy(0, 0, 0)

    def set_to_rpy(self, roll, pitch, yaw):

        cr = math.cos(roll * 0.5)
        sr = math.sin(roll * 0.5)
        cp = math.cos(pitch * 0.5)
        sp = math.sin(pitch * 0.5)
        cy = math.cos(yaw * 0.5)
        sy = math.sin(yaw * 0.5)

        self.w = cr * cp * cy + sr * sp * sy
        self.x = sr * cp * cy - cr * sp * sy
        self.y = cr * sp * cy + sr * cp * sy
        self.z = cr * cp * sy - sr * sp * cy

    def get_rotation_matrix(self):

        return numpy.array(
            [
                [1 - (2 * ((self.y * self.y) + (self.z * self.z))),     2 * ((self.x * self.y) - (self.w * self.z)),      2 * ((self.w * self.y) + (self.x * self.z))],
                [     2 * ((self.x * self.y) + (self.w * self.z)), 1 - (2 * ((self.x * self.x) + (self.z * self.z))),     2 * ((self.y * self.z) - (self.w * self.x))],
                [     2 * ((self.x * self.z) - (self.w * self.y)),      2 * ((self.w * self.x) - (self.y * self.z)), 1 - (2 * ((self.x * self.x) + (self.y * self.y)))],
            ]
        )

    def get_rpy(self):

        return [
            math.atan2(    2 * ((self.w * self.x) + (self.y * self.z)),
                       1 - 2 * ((self.x * self.x) + (self.y * self.y))
                      ),
            (-0.5 * math.pi) + (2 * math.atan2(math.sqrt(1 + 2 * ((self.w * self.y) - (self.x * self.z))),
                                          math.sqrt(1 - 2 * ((self.w * self.y) - (self.x * self.z)))
                                       )),
            math.atan2(    2 * ((self.w * self.z) + (self.x * self.y)),
                       1 - 2 * ((self.y * self.y) + (self.z * self.z))),
        ]

    def get_vector(self):
        return [self.w, self.x, self.y, self.z]

    def rotate_by_rpy(self, roll, pitch, yaw):

        new_rotation = Quaternion()
        new_rotation.set_to_rpy(roll, pitch, yaw)

        new_w = (self.w * new_rotation.w) - (self.x * new_rotation.x) - (self.y * new_rotation.y) - (self.z * new_rotation.z)
        new_x = (self.w * new_rotation.x) + (self.x * new_rotation.w) + (self.y * new_rotation.z) - (self.z * new_rotation.y)
        new_y = (self.w * new_rotation.y) - (self.x * new_rotation.z) + (self.y * new_rotation.w) + (self.z * new_rotation.x)
        new_z = (self.w * new_rotation.z) + (self.x * new_rotation.y) - (self.y * new_rotation.x) + (self.z * new_rotation.w)

        self.w = new_w
        self.x = new_x
        self.y = new_y
        self.z = new_z

class Dynamics:

    X_AXIS = 0
    Y_AXIS = 1
    Z_AXIS = 2
    ROLL   = 0
    PITCH  = 1
    YAW    = 2

    INITIAL_ROLL  = 0
    INITIAL_PITCH = 0
    INITIAL_YAW   = 0

    def __init__(self, point_mass_kg, moment_arm_m, thickness):

        self.point_mass_kg = point_mass_kg
        self.moment_arm_m  = moment_arm_m

        self.velocity     = [0, 0, 0]
        self.position     = [0, 0, 0]
        self.acceleration = [0, 0, 0]

        self.angular_acceleration        = [0, 0, 0]
        self.angular_rates               = [0, 0, 0]
        self.universe_to_body_quaternion = Quaternion()

        # Treating the quadcopter as if it is a uniform cylinder. I did consider spherical :)
        # https://en.wikipedia.org/wiki/List_of_moments_of_inertia
        self.moment_of_inertia = (1.0/12) * self.point_mass_kg * ((3 * self.moment_arm_m * self.moment_arm_m) + (thickness * thickness))

    def arrest(self):

        for dim in range(0, UNIVERSE_DIMENSIONS):
            self.acceleration[dim]         = 0
            self.velocity[dim]             = 0
            self.angular_rates[dim]        = 0
            self.angular_acceleration[dim] = 0

        self.universe_to_body_quaternion.set_to_rpy(0, 0, 0)

    def update_positional_dynamics(self, force):

        for dim in range(0, UNIVERSE_DIMENSIONS):
            self.acceleration[dim]  = force[dim] / self.point_mass_kg
            self.velocity[dim]     += self.acceleration[dim] * UNIVERSE_SIMULATION_PERIOD_S
            self.position[dim]     += self.velocity[dim]     * UNIVERSE_SIMULATION_PERIOD_S

        if self.position[Dynamics.Z_AXIS] <= 0:
            self.position[Dynamics.Z_AXIS] = 0
            self.arrest()

    def update_rotational_dynamics(self, force):

        updated_rotation = []

        for dim in range(0, UNIVERSE_DIMENSIONS):
            torque                          = force[dim] * self.moment_arm_m
            self.angular_acceleration[dim]  = torque / self.moment_of_inertia
            self.angular_rates[dim]        += self.angular_acceleration[dim] * UNIVERSE_SIMULATION_PERIOD_S
            updated_rotation.append(self.angular_rates[dim] * UNIVERSE_SIMULATION_PERIOD_S)

        self.universe_to_body_quaternion.rotate_by_rpy(updated_rotation[Dynamics.ROLL],
                                                       updated_rotation[Dynamics.PITCH],
                                                       updated_rotation[Dynamics.YAW])
class Controller:

    def __init__(self, proportional_gain, integral_gain, derivative_gain, integral_threshold):

        self.proportional_gain  = proportional_gain
        self.integral_gain      = integral_gain
        self.derivative_gain    = derivative_gain
        self.integral_threshold = integral_threshold
        self.integrator         = 0
        self.last_term          = 0

    def set_proportional_gain(self, gain):
        self.proportional_gain = gain

    def step(self, error):
        if (abs(error) < self.integral_threshold):
            self.integrator += error
        else:
            self.integrator = 0
        
        controller_response = error * self.proportional_gain
        controller_response += self.integrator * self.integral_gain * UNIVERSE_SIMULATION_PERIOD_S
        controller_response += (self.last_term - error) * self.derivative_gain / UNIVERSE_SIMULATION_PERIOD_S

        return controller_response

class Rotor:

    RPM_PER_SECOND_WINDUP = 5000
    MAX_RPM               = 10000
    MIN_RPM               = 0
    MAX_POWER             = 100.0
    # Wayne would kill me if he saw this. I have no idea if this is true, but
    # I'm going to assume that it is. Basically I'm assuming that all four
    # rotors at a specific power level are able to make the drone hover
    HOVER_POWER_LEVEL     = 50.0
    MAXIMUM_THRUST_N      = (((STANDARD_GRAVITY * 0.057) / 4) / (HOVER_POWER_LEVEL / MAX_POWER))
    THRUST_N_PER_RPM      = MAXIMUM_THRUST_N / MAX_RPM
    
    def __init__(self):
        self.target_rpm  = 0
        self.current_rpm = 0

    def set_thrust_output(self, force_n):

        rpm = force_n / Rotor.THRUST_N_PER_RPM

        # Cap the target RPM
        if rpm > Rotor.MAX_RPM:
            rpm = Rotor.MAX_RPM
        elif rpm < Rotor.MIN_RPM:
            rpm = Rotor.MIN_RPM
        self.target_rpm = rpm

    def step(self):
        # Wind up at the rate we set. If we were to go beyond the target, simply snap to that speed
        if self.current_rpm < self.target_rpm:
            self.current_rpm += Rotor.RPM_PER_SECOND_WINDUP * UNIVERSE_SIMULATION_PERIOD_S
            if self.current_rpm > self.target_rpm:
                self.current_rpm = self.target_rpm

        elif self.current_rpm > self.target_rpm:
            self.current_rpm -= Rotor.RPM_PER_SECOND_WINDUP * UNIVERSE_SIMULATION_PERIOD_S
            if self.current_rpm < self.target_rpm:
                self.current_rpm = self.target_rpm

    def get_thrust(self):
        return self.current_rpm * Rotor.THRUST_N_PER_RPM

    def get_cur_tar(self):
        return [self.current_rpm, self.target_rpm]

class Telemetry:

    POSITION_INDEX     = 0
    VELOCITY_INDEX     = 1
    ACCELERATION_INDEX = 2
    
    MAX_TELEMETRY_TIME_S = 120

    COLOR_SEQUENCE = [
        'red',
        'green',
        'blue',
        'purple'
    ]

    class N_plot:
        def __init__(self, num_points, num_measurements, name, y_label, measurement_labels = None):
            self.measurements = []
            for i in range(0, num_measurements):
                buffered_list = [None]*num_points
                self.measurements.append(buffered_list)
            self.count = 0
            self.name = name
            self.y_label = y_label
            self.measurement_labels = measurement_labels

        def append(self, data):
            for i in range(0, len(self.measurements)):
                self.measurements[i][self.count] = data[i]
            self.count += 1

        def draw(self, plot, time):
            for i in range(0, len(self.measurements)):
                plot.plot(time, self.measurements[i], color=Telemetry.COLOR_SEQUENCE[i])
            plot.grid(True)
            plot.set_xlabel("Time (S)")
            plot.set_ylabel(self.y_label)
            plot.set_title(self.name)

    def __init__(self):

        timesteps = int(Telemetry.MAX_TELEMETRY_TIME_S / SIM_LOG_PERIOD_S)

        self.times = [None] * timesteps
        self.count = 0

        self.linear_data = [
            Telemetry.N_plot(timesteps, 3, "Linear Acceleration", "Meters / Second ^ 2"),
            Telemetry.N_plot(timesteps, 3, "Linear Velocity",     "Meters / Second"),
            Telemetry.N_plot(timesteps, 3, "Linear Position",     "Meters")
        ]

        self.angular_data = [
            Telemetry.N_plot(timesteps, 3, "Angular Acceleration", "Radians / Second ^ 2"),
            Telemetry.N_plot(timesteps, 3, "Angular Rates",        "Radians / Second"),
            Telemetry.N_plot(timesteps, 3, "Angular Position",     "Radians")
        ]

        self.rotor_data = [
            Telemetry.N_plot(timesteps, 2, "Rotor Front Left", "RPM"),
            Telemetry.N_plot(timesteps, 2, "Rotor Front Right", "RPM"),
            Telemetry.N_plot(timesteps, 2, "Rotor Rear Left", "RPM"),
            Telemetry.N_plot(timesteps, 2, "Rotor Rear Right", "RPM")
        ]

        self.quaternion            = Telemetry.N_plot(timesteps, 4, "Universe -> Body Quaternion", "")
        self.desired_pointing      = Telemetry.N_plot(timesteps, 3, "Desired Linear Velocities", "")
        self.roll_desired_pointing = Telemetry.N_plot(timesteps, 2, "Desired Pointing Angles", "")
        self.roll_desired_vel      = Telemetry.N_plot(timesteps, 2, "Desired Angular Rates", "")

    def add_data(self, time, linear_pos, linear_vel, linear_acl, angular_pos, angular_vel, angular_acl, rotor_fl, rotor_fr, rotor_rl, rotor_rr, quaternion, desired_pointing, roll_desired_pointing, roll_desired_vel):

        self.linear_data[2].append(linear_pos)
        self.linear_data[1].append(linear_vel)
        self.linear_data[0].append(linear_acl)

        self.angular_data[2].append(angular_pos)
        self.angular_data[1].append(angular_vel)
        self.angular_data[0].append(angular_acl)

        self.rotor_data[0].append(rotor_fl)
        self.rotor_data[1].append(rotor_fr)
        self.rotor_data[2].append(rotor_rl)
        self.rotor_data[3].append(rotor_rr)

        self.quaternion.append(quaternion)
        self.desired_pointing.append(desired_pointing)

        self.roll_desired_pointing.append(roll_desired_pointing)
        self.roll_desired_vel.append(roll_desired_vel)

        self.times[self.count] = time
        self.count += 1

    def plot(self):
        figure, axis = plt.subplots(4, 4, sharex=True)

        for i in range(0, len(self.linear_data)):
            self.linear_data[i].draw(axis[0, i], self.times)

        for i in range(0, len(self.angular_data)):
            self.angular_data[i].draw(axis[1, i], self.times)
        self.quaternion.draw(axis[0,3], self.times)
        self.desired_pointing.draw(axis[1,3], self.times)

        for i in range(0, len(self.rotor_data)):
            self.rotor_data[i].draw(axis[3, i], self.times)

        self.roll_desired_pointing.draw(axis[2, 2], self.times)
        self.roll_desired_vel.draw(axis[2, 1], self.times)

        plt.show()

class Drone:

    CODRONE_MASS_KG        = 0.057
    CODRONE_HALF_WIDTH_M   = 0.138 / 2
    CODRONE_HEIGHT_M       = 0.054
    MAX_POINTING_ANGLE_DEG = 20 # Theoretical limit without loosing altitude is 30 degrees, want to give some margin for error
    MAX_POINTING_ANGLE_RAD = math.radians(MAX_POINTING_ANGLE_DEG)
    MAX_ANGULAR_VELOCITY_DEG_PER_S = 10
    MAX_ANGULAR_VELOCITY_RAD_PER_S = math.radians(MAX_ANGULAR_VELOCITY_DEG_PER_S)
    MAX_VELOCITY_M_PER_S   = 1
    MAX_POWER              = 100
    MAX_POSITIVE_POWER     = MAX_POWER
    MAX_NEGATIVE_POWER     = -1 * MAX_POWER

    ROLL_PITCH_CONTROLLERS_GAIN_SCHEDULE = [
        [0.25, 0, 0, 0],
        [10,   0, 0, 0],
        [0.1,  0, 0, 0]
    ]
    ROLL_PITCH_CONTROLLERS_LINEAR_VELOCITY_INDEX  = 0
    ROLL_PITCH_CONTROLLERS_ANGULAR_POINTING_INDEX = 1
    ROLL_PITCH_CONTROLLERS_ANGULAR_RATE_INDEX     = 2

    ROLL_PITCH_MAP = [
        {"LINEAR_AXIS" : Dynamics.Y_AXIS, "ANGULAR_AXIS" : Dynamics.ROLL}, 
        {"LINEAR_AXIS" : Dynamics.X_AXIS, "ANGULAR_AXIS" : Dynamics.PITCH}, 
    ]

    PROPORTIONAL_GAIN_INDEX  = 0
    INTEGRAL_GAIN_INDEX      = 1
    DERIVATIVE_GAIN_INDEX    = 2
    INTEGRAL_THRESHOLD_INDEX = 3

    THROTTLE_CONTROLLER_PROPORTIONAL_GAIN  = 0.37
    THROTTLE_CONTROLLER_INTEGRAL_GAIN      = 0.01
    THROTTLE_CONTROLLER_DERIVATIVE_GAIN    = 0
    THROTTLE_CONTROLLER_INTEGRAL_THRESHOLD = 0.1

    ROTOR_FORWARD_LEFT_INDEX  = 0
    ROTOR_FORWARD_RIGHT_INDEX = 1
    ROTOR_REAR_LEFT_INDEX     = 2
    ROTOR_REAR_RIGHT_INDEX    = 3
    NUM_ROTORS                = 4

    def __init__(self):
        self.rotors = []
        for i in range(0, Drone.NUM_ROTORS):
            self.rotors.append(Rotor())

        self.dynamics         = Dynamics(Drone.CODRONE_MASS_KG, Drone.CODRONE_HALF_WIDTH_M, Drone.CODRONE_HEIGHT_M)
        self.desired_angles   = [0, 0, 0]
        self.desired_throttle = 0
        self.desired_velocities = [0, 0, 0]

        self.unit_modifiers = {
            "m" :  1,
            "cm" : CM_PER_METER,
            "ft" : FEET_PER_METER,
            "in" : INCHES_PER_METER
        }

        self.roll_pitch_controllers = []
        for dim in range(0, UNIVERSE_DIMENSIONS - 1):
            stages = []
            for stage in range(0, len(Drone.ROLL_PITCH_CONTROLLERS_GAIN_SCHEDULE)):
                stages.append(Controller(Drone.ROLL_PITCH_CONTROLLERS_GAIN_SCHEDULE[stage][Drone.PROPORTIONAL_GAIN_INDEX],
                                         Drone.ROLL_PITCH_CONTROLLERS_GAIN_SCHEDULE[stage][Drone.INTEGRAL_GAIN_INDEX],
                                         Drone.ROLL_PITCH_CONTROLLERS_GAIN_SCHEDULE[stage][Drone.DERIVATIVE_GAIN_INDEX],
                                         Drone.ROLL_PITCH_CONTROLLERS_GAIN_SCHEDULE[stage][Drone.INTEGRAL_THRESHOLD_INDEX]))
            self.roll_pitch_controllers.append(stages)

        self.throttle_controller = Controller(Drone.THROTTLE_CONTROLLER_PROPORTIONAL_GAIN,
                                              Drone.THROTTLE_CONTROLLER_INTEGRAL_GAIN,
                                              Drone.THROTTLE_CONTROLLER_DERIVATIVE_GAIN,
                                              Drone.THROTTLE_CONTROLLER_INTEGRAL_THRESHOLD)

        self.roll_velocity_controller = Controller(0.25, 0, 0, 0)
        self.roll_pointing_controller = Controller(10,   0, 0, 0)
        self.roll_a_vel_controller    = Controller(0.1,  0, 0, 0)

        self.str_count = 0

        self.altitudes = []
        self.velocities = []
        self.accelerations = []
        self.times = []
        self.errors = []

        self.full_sim_time = 0

        self.last_flow_x = 0
        self.last_flow_y = 0

        self.telemetry = Telemetry()

    def crash(self):
        self.telemetry.plot()
        exit(-1)

    def open(self):
        # We're going to do nothing here
        pass

    def close(self):
        self.telemetry.plot()

    def reset_sensor(self):
        pass

    def takeoff(self):
        pass

    def get_elevation(self, unit="m"):
        return self.dynamics.position[Dynamics.Z_AXIS] * self.unit_modifiers[unit]

    def get_front_range(self, unit="cm"):
        # Pretend we're in an infinitly open room with a single infinite wall
        # that we're always facing and assume this wall is 10 meters from the
        # starting position
        return (10 - self.dynamics.position[Dynamics.X_AXIS]) * self.unit_modifiers[unit]

    def get_flow_x(self, unit="cm"):
        distance_since_last = self.dynamics.position[Dynamics.X_AXIS] - self.last_flow_x
        self.last_flow_x = self.dynamics.position[Dynamics.X_AXIS]
        return distance_since_last * self.unit_modifiers[unit]

    def get_flow_y(self, unit="cm"):
        distance_since_last = self.dynamics.position[Dynamics.Y_AXIS] - self.last_flow_y
        self.last_flow_y = self.dynamics.position[Dynamics.Y_AXIS]
        return distance_since_last * self.unit_modifiers[unit]

    def hover(self, duration):
        self.go(0, 0, 0, 0, duration)
        pass

    def turn_left(self, degrees):
        pass

    def land(self):
        # Just slam into the ground as fast as we can
        self.go(0, 0, 0, -100, 100)

    def power_limit(self, power):
        if power > Drone.MAX_POSITIVE_POWER:
            power = Drone.MAX_POSITIVE_POWER
        elif power < Drone.MAX_NEGATIVE_POWER:
            power = Drone.MAX_NEGATIVE_POWER
        return power

    def set_angle_power(self, axis, power):

        # For RPY commands, a positive power puts us in the positive direction along that axis. This is reverse from the way we need to tilt, so flip the desired angle
        self.desired_velocities[axis] = Drone.MAX_VELOCITY_M_PER_S * self.power_limit(power) / Drone.MAX_POWER

    def go(self, roll, pitch, yaw, throttle, duration):

        self.set_angle_power(Dynamics.Y_AXIS, roll)
        self.set_angle_power(Dynamics.X_AXIS, pitch)
        self.set_angle_power(Dynamics.Z_AXIS, yaw)
        self.desired_throttle = throttle

        self.move(duration)

    def move(self, duration):

        self.desired_velocities[Dynamics.Z_AXIS] = Drone.MAX_VELOCITY_M_PER_S * (self.power_limit(self.desired_throttle) / Drone.MAX_POWER)

        liftoff = False

        current_sim_time_s = 0
        next_log_time = current_sim_time_s + SIM_LOG_PERIOD_S
        while(current_sim_time_s < duration):

            # Step forward our time estimate
            current_sim_time_s += UNIVERSE_SIMULATION_PERIOD_S
            self.full_sim_time += UNIVERSE_SIMULATION_PERIOD_S

            # Step 1: Calculate the positional dynamics
            total_thrust = 0
            for rotor in self.rotors:
                total_thrust += rotor.get_thrust()

            # Rotate the thrust vector by the current vehicle orientation. We
            # assume that the thrust vector relative to the body is only on
            # the +Z axis
            universe_thrust_vector = numpy.matmul(self.dynamics.universe_to_body_quaternion.get_rotation_matrix(),
                                                  numpy.array([0, 0, 1]))

            # Figure out what the thrust in each direction has been
            net_forces = [0, 0, 0]
            for dim in range(0, UNIVERSE_DIMENSIONS):
                net_forces[dim] = universe_thrust_vector[dim] * total_thrust

            # Always subtract out the force due to gravity
            net_forces[dim] -= (Drone.CODRONE_MASS_KG * STANDARD_GRAVITY)

            # Apply the net forces on the body
            self.dynamics.update_positional_dynamics(net_forces)

            # Figure out if we've lifted off the ground
            if (self.dynamics.position[Dynamics.Z_AXIS] > 0) and liftoff == False:
                liftoff = True

            # Step 2: Calculate rotational dynamics
            self.dynamics.update_rotational_dynamics(
                [
                    (self.rotors[Drone.ROTOR_FORWARD_RIGHT_INDEX].get_thrust() + self.rotors[Drone.ROTOR_REAR_RIGHT_INDEX].get_thrust()) - (self.rotors[Drone.ROTOR_FORWARD_LEFT_INDEX].get_thrust() + self.rotors[Drone.ROTOR_REAR_LEFT_INDEX].get_thrust()),
                    (self.rotors[Drone.ROTOR_FORWARD_RIGHT_INDEX].get_thrust() + self.rotors[Drone.ROTOR_FORWARD_LEFT_INDEX].get_thrust()) - (self.rotors[Drone.ROTOR_REAR_RIGHT_INDEX].get_thrust() + self.rotors[Drone.ROTOR_REAR_LEFT_INDEX].get_thrust()),
                    0   # We're ignoring yaw for now
                ]
            )

            # Step 3: Calculate new rotor commands

            requested_roll_thrust     = [0, 0, 0, 0]
            requested_pitch_thrust    = [0, 0, 0, 0]
            requested_yaw_thrust      = [0, 0, 0, 0]
            requested_throttle_thrust = [0, 0, 0, 0]

            # Roll Axis Commands. We need to utilize a multi-stage control system here to keep things in check
            # Stage 1: Linear Velocity -> Never go faster than max allowable velocity in any direction
            # Stage 2: Desired Pointing -> Control effort from linear velocity. If we're at the velocity we want, our pointing should be 0
            # Stage 3: Desired Angular Velocity -> If we're at the pointing angle we want, our body rates should be zero
            # I won't defend this. This is just word soup. Just soup.

            roll_pitch_desired_pointing      = [0, 0]
            roll_pitch_desired_angular_rates = [0, 0]
            roll_pitch_efforts               = [0, 0]

            for dim in range(0, UNIVERSE_DIMENSIONS - 1):

                linear_axis = Drone.ROLL_PITCH_MAP[dim]["LINEAR_AXIS"]
                angular_axis = Drone.ROLL_PITCH_MAP[dim]["ANGULAR_AXIS"]

                linear_velocity_error = self.desired_velocities[linear_axis] - self.dynamics.velocity[linear_axis]
                roll_pitch_desired_pointing[dim] = self.roll_pitch_controllers[dim][Drone.ROLL_PITCH_CONTROLLERS_LINEAR_VELOCITY_INDEX].step(linear_velocity_error)

                if roll_pitch_desired_pointing[dim] > Drone.MAX_POINTING_ANGLE_RAD:
                    roll_pitch_desired_pointing[dim] = Drone.MAX_POINTING_ANGLE_RAD
                elif roll_pitch_desired_pointing[dim] < (-1 * Drone.MAX_POINTING_ANGLE_RAD):
                    roll_pitch_desired_pointing[dim] = -1 * Drone.MAX_POINTING_ANGLE_RAD

                if linear_axis == Dynamics.Y_AXIS:
                    roll_pitch_desired_pointing[dim] *= -1  

                pointing_error = roll_pitch_desired_pointing[dim] - self.dynamics.universe_to_body_quaternion.get_rpy()[angular_axis]
                roll_pitch_desired_angular_rates[dim] = self.roll_pitch_controllers[dim][Drone.ROLL_PITCH_CONTROLLERS_ANGULAR_POINTING_INDEX].step(pointing_error)

                if roll_pitch_desired_angular_rates[dim] > Drone.MAX_ANGULAR_VELOCITY_RAD_PER_S:
                    roll_pitch_desired_angular_rates[dim] = Drone.MAX_ANGULAR_VELOCITY_RAD_PER_S
                elif roll_pitch_desired_angular_rates[dim] < (-1 * Drone.MAX_ANGULAR_VELOCITY_RAD_PER_S):
                    roll_pitch_desired_angular_rates[dim] = -1 * Drone.MAX_ANGULAR_VELOCITY_RAD_PER_S

                angular_rate_error = roll_pitch_desired_angular_rates[dim] - self.dynamics.angular_rates[angular_axis]
                roll_pitch_efforts[dim] = self.roll_pitch_controllers[dim][Drone.ROLL_PITCH_CONTROLLERS_ANGULAR_RATE_INDEX].step(angular_rate_error)

            # Build up the roll & pitch thrust efforts
            requested_roll_thrust[Drone.ROTOR_FORWARD_LEFT_INDEX]  = -1 * roll_pitch_efforts[Dynamics.ROLL]
            requested_roll_thrust[Drone.ROTOR_REAR_LEFT_INDEX]     = -1 * roll_pitch_efforts[Dynamics.ROLL]
            requested_roll_thrust[Drone.ROTOR_FORWARD_RIGHT_INDEX] = roll_pitch_efforts[Dynamics.ROLL]
            requested_roll_thrust[Drone.ROTOR_REAR_RIGHT_INDEX]    = roll_pitch_efforts[Dynamics.ROLL]

            requested_pitch_thrust[Drone.ROTOR_FORWARD_LEFT_INDEX]  = roll_pitch_efforts[Dynamics.PITCH]
            requested_pitch_thrust[Drone.ROTOR_FORWARD_RIGHT_INDEX] = roll_pitch_efforts[Dynamics.PITCH]
            requested_pitch_thrust[Drone.ROTOR_REAR_LEFT_INDEX]     = -1 * roll_pitch_efforts[Dynamics.PITCH]
            requested_pitch_thrust[Drone.ROTOR_REAR_RIGHT_INDEX]    = -1 * roll_pitch_efforts[Dynamics.PITCH]

            # Throttle Axis Commands. Using the thrust vector to the universe, and the
            # reference throttle power, figure out out much we must increase
            # the base reference thrust

            throttle_error = self.desired_velocities[Dynamics.Z_AXIS] - self.dynamics.velocity[Dynamics.Z_AXIS]
            z_thrust = self.throttle_controller.step(throttle_error)

            throttle_thrust = (Drone.CODRONE_MASS_KG * STANDARD_GRAVITY) + z_thrust

            for rotor_id in range(0, Drone.NUM_ROTORS):
                requested_throttle_thrust[rotor_id] = (throttle_thrust / universe_thrust_vector[Dynamics.Z_AXIS]) / Drone.NUM_ROTORS

            # Set the new rotor thrust commands
            for rotor_id in range(0, Drone.NUM_ROTORS):
                self.rotors[rotor_id].set_thrust_output(requested_roll_thrust[rotor_id] + 
                                                        requested_pitch_thrust[rotor_id] + 
                                                        requested_yaw_thrust[rotor_id] + 
                                                        requested_throttle_thrust[rotor_id])
                self.rotors[rotor_id].step()

            if current_sim_time_s >= next_log_time:
                next_log_time += SIM_LOG_PERIOD_S
                try:
                    self.telemetry.add_data(
                        self.full_sim_time,
                        self.dynamics.position,
                        self.dynamics.velocity,
                        self.dynamics.acceleration,
                        self.dynamics.universe_to_body_quaternion.get_rpy(),
                        self.dynamics.angular_rates,
                        self.dynamics.angular_acceleration,
                        self.rotors[Drone.ROTOR_FORWARD_LEFT_INDEX].get_cur_tar(),
                        self.rotors[Drone.ROTOR_FORWARD_RIGHT_INDEX].get_cur_tar(),
                        self.rotors[Drone.ROTOR_REAR_LEFT_INDEX].get_cur_tar(),
                        self.rotors[Drone.ROTOR_REAR_RIGHT_INDEX].get_cur_tar(),
                        self.dynamics.universe_to_body_quaternion.get_vector(),
                        self.desired_velocities,
                        roll_pitch_desired_pointing,
                        roll_pitch_desired_angular_rates
                    )
                except:
                    # Force crash the drone
                    print("Sim execution time reached")
                    self.crash()

            # Perform an early return if we hit the ground
            if self.dynamics.position[Dynamics.Z_AXIS] <= 0 and liftoff:
                print("Drone crashed into the ground")
                self.crash()

if __name__ == '__main__':

    # drone = Drone(1, 0, 0, 1000, 10, 0, 0, 1000, 0.1, 0, 0, 1000)
    drone = Drone()

    print("Starting simulation")

    start = time.time()

    # drone.go(0, 0, 0, 100, 3)
    # drone.go(0, 0, 0, 0, 3)
    # drone.go(100, 0, 0, 0, 6)
    # drone.go(-100, 0, 0, 0, 6)
    # drone.go(0, 0, 0, 0, 3)
    # drone.go(0, 100, 0, 0, 6)
    # drone.go(0, -100, 0, 0, 6)
    # drone.go(0, 0, 0, 0, 3)

    
    drone.go(0, 0, 0, 100, 5)
    drone.go(0, 0, 0, 0, 5)

    end = time.time()

    print("Simulation complete in {:.1f} seconds".format(end - start))

    drone.close()
