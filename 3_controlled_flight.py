# A functional problem that has persisted through the programs so far is that
# they rely on different conditionals for the directions traveled and they only
# guarantee that you have gone past that desired target. I referenced earlier
# my distaste for control theory, and now we're gonna pull from it. This is a
# more advanced topic, but I have my reasons... I'll explain more in the next
# program

import codrone_edu
# import mock_drone
drone = codrone_edu.drone.Drone()
# drone = mock_drone.Drone()
drone.open()

# Classes are a combination of data structures and algorithms. In this case I
# want to create a strong linkage between the algorithm used in a
# proportional-integral controller and the data required to make that go,
# namely the integration term
class Controller:
    # These two gain values are arbitrary, but usually you want proportional
    # gain to be at least an order of magnitude above integral
    PROPORTIONAL_GAIN  = 0.75
    INTEGRAL_GAIN      = 0
    INTEGRAL_THRESHOLD = 5

    # Some special Python incantations. This function must exist and any
    # functions declared as part of Controller must take the "self" variable
    # as the very first argument in the function list


    # PI controller crash course: The basic theory is that we have some
    # arbitrary input that we want to correlate to our arbitrary response.You
    # already did this with your original while loops: while you still had
    # distance to go, keep moving in that direction. While you always had a
    # constant reponse level (50% power) here the higher the distance
    # remaining, the stronger our response will be. In turn, as we get closer
    # and have less distance to cover, our output power will go down (hence
    # _proportional_ control). One nasty habit of pure proportional controllers
    # is that as you near your target, the generated response is so low that
    # you don't actually move closer. Thats where the integral term comes in:
    # if you've been away from the target for too long, the output response
    # will get stronger, ideally giving you that final nudge in the right
    # direction. 

    def generate_response(self, distance_remaining):

        # If we accumulate the distance for the integral when we're far away,
        # we'll hit a phenomenon called "integrator windup" where we'll
        # overshoot out target and keep going while the integral term starts
        # accumulating negative distance remaining. Because we know overshoot
        # can happen, the distance remaining can go negative. So when
        # thresholding the action of the integrator, we want to take the
        # absolute value of the distance remaining

        if (abs(distance_remaining) < Controller.INTEGRAL_THRESHOLD):
            self.integrator += distance_remaining
        else:
            # If we fall outside of the range where we want to enable integral
            # control, reset back to 0, also protecting against wind-up
            self.integrator = 0

        control_response = distance_remaining * Controller.PROPORTIONAL_GAIN
        control_response += self.integrator * Controller.INTEGRAL_GAIN

        return control_response

def go_target(target_distance_inches, axis):

    # Because of how floating point values work in computers, its practically a
    # guarantee that we'll never actually hit our target position. So set a
    # threshold for fuzzy logic

    POSITION_REACHED_THRESHOLD_INCHES = 0.25
    POWER_OUTPUT_CAP                  = 100
    FORWARD_POWER_OUTPUT_CAP          = POWER_OUTPUT_CAP
    REVERSE_POWER_OUTPUT_CAP          = -1 * POWER_OUTPUT_CAP
    INCHES_PER_FEET                   = 12
    MOVEMENT_DURATION_S               = 0.25    # I decreased this to help with controller response

    target_reached = False

    power_controller = Controller()

    while(not target_reached):

        # Declare these here and re-initialize them every loop
        roll_power     = 0
        pitch_power    = 0
        yaw_power      = 0
        throttle_power = 0

        # Because we're generalizing our control response to allow for forward
        # and backward motion, we now only care about which axis to measure on
        current_distance_in = 0
        if axis == "Z":
            current_distance_in = drone.get_elevation("ft") * INCHES_PER_FEET
        elif axis == "X":
            current_distance_in = drone.get_front_range("in")

        distance_remaining = target_distance_inches - current_distance_in

        if(abs(distance_remaining) < POSITION_REACHED_THRESHOLD_INCHES):
            target_reached = True
        else:

            movement_power_level = power_controller.generate_response(distance_remaining)

            # Limit the power level
            if (movement_power_level > FORWARD_POWER_OUTPUT_CAP):
                movement_power_level = FORWARD_POWER_OUTPUT_CAP
            elif (movement_power_level < REVERSE_POWER_OUTPUT_CAP):
                movement_power_level = REVERSE_POWER_OUTPUT_CAP

            if axis == "Z":
                throttle_power = movement_power_level
            elif axis == "X":
                # We need to flip this here since our sensor coordinate grid is mirrored. As we make movements in the +X direction, the distance from the wall gets smaller, meaning our coordinate relative to the wall gets smaller. The controller expects that a positive direction movement results in a larger coordinate, so we have to flip that expectation
                pitch_power = -1 * movement_power_level
            
            drone.go(roll_power, pitch_power, yaw_power, throttle_power, MOVEMENT_DURATION_S)

            # Adding in the movement response so you can see how it changes across time
            print("{} -> {:5.1f} & {:5.1f}".format(axis, current_distance_in, movement_power_level))

# Basically same as before
drone.reset_sensor()
drone.takeoff()
drone.reset_sensor()

# This function only takes in inches for the distance argument, so we have to
# multiply our old 9 to this
go_target(9 * 12, "Z") 

go_target(30, "X")

drone.turn_left(180)
go_target(30, "X")

drone.turn_left(180)
go_target(30, "X")

# This function call will still make the drone move backwards, but we no longer
# have to specify the motion and the axis anymore. We just do position on the
# axis and it will go there, regardless of where the drone currently is on the
# axis
go_target(78, "X")

print("Land {:5.1f}".format(drone.get_front_range("in")))

drone.land()
drone.close()